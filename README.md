# BatteryJuice

Print the charging level of the Librem 5 (or any other battery powered Linux device) in intervals.

Intervals are either 10 seconds or one minute.

On the Librem 5, the granularity of the charge level seems to be 3mAh, and when idle, that leads to a changed reading every 30...40s.

In other words, with a 10 second interval, the difference is either 0 or 3mAh. In a minute, it's either 3mAh or 6mAh.

Consequently, when measuring an idle device, choose the minute interval
`./batteryjuice -w`
and wait 10 minutes.
Every 10 minutes, an accumulated difference is printed.
